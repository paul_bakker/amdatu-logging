package org.amdatu.logging.logwriters.syslog;

import java.io.IOException;
import java.text.MessageFormat;

import javax.print.attribute.standard.Severity;

import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.dm.annotation.api.Start;
import org.apache.felix.dm.annotation.api.Stop;
import org.osgi.service.log.LogEntry;
import org.osgi.service.log.LogListener;
import org.osgi.service.log.LogReaderService;

import com.cloudbees.syslog.Facility;
import com.cloudbees.syslog.sender.UdpSyslogMessageSender;

@Component
public class SysLogForwarder {
	@ServiceDependency
	private volatile LogReaderService m_reader;
	private volatile MyListener m_listener;
	
	@Start
	public void start() {
		m_listener = new MyListener();
		m_reader.addLogListener(m_listener);
	}
	
	@Stop
	public void stop() {
		m_reader.removeLogListener(m_listener);
	}
	
	class MyListener implements LogListener {
		@Override
		public void logged(LogEntry entry) {
			// Initialise sender
			UdpSyslogMessageSender messageSender = new UdpSyslogMessageSender();
			messageSender.setSyslogServerHostname("localhost"); // some syslog cloud services may use this field to transmit a secret key
			messageSender.setDefaultAppName("myapp");
			messageSender.setDefaultFacility(Facility.USER);
			messageSender.setSyslogServerPort(5000);

			// send a Syslog message
			try {
				System.out.println(entry.getMessage());
				messageSender.sendMessage(entry.getMessage());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
