package org.amdatu.logging.forwarders.syslog.test;

import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Test;
import org.osgi.service.log.LogService;

import static org.amdatu.testing.configurator.TestConfigurator.*;

public class LogTest {
	
	private volatile LogService m_logService;
	
	@Before
	public void setup() throws InterruptedException {
		configure(this).add(serviceDependency(LogService.class)).apply();
		
		TimeUnit.SECONDS.sleep(2);
	}
	
	@Test
	public void testLogForward() {
		m_logService.log(LogService.LOG_INFO, "Logging test using syslog");
	}
}
